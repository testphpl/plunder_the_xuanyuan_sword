// GameObject.cpp: implementation of the CGameObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Game.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGameObject::CGameObject( CGameMap* pGameMap )
{
	m_pGameMap = pGameMap;
	m_hImageDC = NULL;
	m_bDeleteImage = FALSE;
}

CGameObject::~CGameObject()
{
	m_pGameMap = NULL;

	if( m_bDeleteImage && (m_hImageDC != NULL) )
	{
		::DeleteDC( m_hImageDC );
		m_hImageDC = NULL;
	}
}

// 初始化图片设备
BOOL CGameObject::InitImage( char* psFileName )
{
	// 打开瓷砖位图
	HBITMAP hBmp=(HBITMAP)LoadImage(NULL,psFileName,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	if( hBmp == NULL )
		return FALSE;

	HDC hdc = ::GetDC( theApp.m_pMainWnd->m_hWnd );
	m_hImageDC = ::CreateCompatibleDC( hdc );
	DeleteObject( ::SelectObject( m_hImageDC, hBmp ) );
	ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );

	m_bDeleteImage = TRUE;
	return TRUE;
}

// 设置位置（左上角点的位置）
BOOL CGameObject::SetPos( int x, int y )
{
	m_x = x;
	m_y = y;
	return TRUE;
}

// 绘制
void CGameObject::Draw( HDC hDC, int x, int y )
{
}

// 获得所在格子的坐标。每个精灵只能占一个格子的位置
// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
BOOL CGameObject::GetGride( int* pX, int* pY )
{
	return TRUE;
}

// 移动
void CGameObject::Move( long lNow )
{
}

// 判断自己是否可以被显示
BOOL CGameObject::CanBeShown()
{
	int map_left = m_pGameMap->m_nLeft;
	int map_top = m_pGameMap->m_nTop;
	int map_width = GAME_AREA_W+TILE_W;
	int map_height = GAME_AREA_H+TILE_H;

	return IsAreaCut( m_x, m_y, m_w, m_h, map_left, map_top, map_width, map_height );
}

// 得到被攻击检测矩形
BOOL CGameObject::GetHitRect( int* x, int* y, int* w, int* h )
{
	return TRUE;
}

// 得到地图占用矩形
BOOL CGameObject::GetPlaceRect( int* px, int* py, int* pw, int* ph )
{
    return TRUE;
}

// 判断是否与另外一个物体相交
BOOL CGameObject::IsObjectCut( CGameObject* pObj )
{
	if( pObj == NULL )
		return FALSE;

	// 获得自己的地图检测矩形
	int x1, y1, w1, h1;
	if( !GetPlaceRect( &x1, &y1, &w1, &h1 ) )
		return FALSE;

	// 获得另一个物体的地图检测矩形
	int x2, y2, w2, h2;
	if( !pObj->GetPlaceRect( &x2, &y2, &w2, &h2 ) )
		return FALSE;

	// 判断自己和另外一个物体是否碰撞
	return IsAreaCut( x1, y1, w1, h1, x2, y2, w2, h2 );
}

// END