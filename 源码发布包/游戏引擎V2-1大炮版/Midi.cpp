// Midi.cpp

#include "stdafx.h"
#include "Game.h"
#include <process.h>   // 多线程支持
#include <dsound.h>    // DirectSound
#pragma comment(lib,"dsound.lib")
#pragma comment(lib,"winmm.lib")

static void Thread( PVOID pvoid )
{
	CMidi* pMidi = (CMidi *)pvoid;

	while( pMidi->m_bRun )
	{
		WaitForSingleObject( pMidi->m_hEvent, INFINITE );

		switch( pMidi->m_nEvent )
		{
		case MIDI_EVENT_PLAY:
			pMidi->DoPlayMidi();
			break;
		case MIDI_EVENT_REPLAY:
			pMidi->DoReplayMidi();
			break;
		case MIDI_EVENT_STOP:
			pMidi->DoStopMidi();
			break;
		case MIDI_EVENT_QUIT:
			pMidi->m_nEvent = 0;
			_endthread();
			break;
		}
		pMidi->m_nEvent = 0;
	}

	pMidi->m_nEvent = 0;
	_endthread();
}

CMidi::CMidi()
{
	m_hWnd = NULL;
	m_wDeviceID = 0x0;//MCI装置代号
	m_dwReturn = 0x0;
	m_psMidiFile = new char[256];
	
	m_bRun = TRUE;
	m_hEvent = CreateEvent( NULL, FALSE, TRUE, NULL );
	m_nEvent = 0;

    // 四个缓冲区，轮流播放
    m_nCurSound = 0;
}

CMidi::~CMidi()
{
	m_bRun = FALSE;
	m_nEvent = MIDI_EVENT_QUIT;
	SetEvent( m_hEvent );

	// 等待线程退出
	int i = 0;
	while( m_nEvent != 0 )
	{
		i ++;
		Sleep( 10 );
		if( i == 3 )
			break;
	}
	CloseHandle( m_hEvent );

	delete [] m_psMidiFile;

    if( m_pDSound != NULL )
    {
        for( int i=0; i<MAX_SOUND; i++ )
        {
            if( m_pBufferArray[i] != NULL )
            {
                m_pBufferArray[i]->Release();
                m_pBufferArray[i] = NULL;
            }
        }
    }
}

void CMidi::Init(HWND hWnd)
{
    // 启动播放背景MIDI的线程
	_beginthread( Thread, 0, this );

	m_hWnd = hWnd;

    // 与DirectSound有关的变量
    for( int i=0; i<MAX_SOUND; i++ )
        m_pBufferArray[i] = NULL;
    m_pDSound = NULL;
    m_pWave   = NULL;
    m_bDSoundOK = FALSE;
    ////
    // 与DirectSound有关的初始化操作
    HRESULT result = DirectSoundCreate( NULL, &m_pDSound, NULL );
    if( result != DS_OK )
    {
		MessageBox(hWnd,DSOUND_FAILURE,"error",MB_OK);

        return;
    }

    result = m_pDSound->SetCooperativeLevel( hWnd, DSSCL_NORMAL );
    if( result != DS_OK )
    {
		MessageBox(hWnd,DSOUND_FAILURE,"error",MB_OK);

        return;
    }

    m_bDSoundOK = TRUE;
}

// 由于打开MCI设备需要一定的时间，所以开一个线程去做这个工作
void CMidi::PlayMidi(char* psMidiFile)
{
    strncpy( m_psMidiFile, psMidiFile, 255 );

	m_nEvent = MIDI_EVENT_PLAY;
	SetEvent( m_hEvent );
}

void CMidi::ReplayMidi(WPARAM w)
{
	if( m_wDeviceID!=0 && w==MCI_NOTIFY_SUCCESSFUL )
	{
		m_nEvent = MIDI_EVENT_REPLAY;
		SetEvent( m_hEvent );
	}
}

void CMidi::StopMidi()
{
	m_nEvent = MIDI_EVENT_STOP;
	SetEvent( m_hEvent );
}

//
DWORD CMidi::DoPlayMidi()
{
	DoStopMidi();

	char temp[256];

	m_mciOpenParms.lpstrDeviceType = "sequencer";
	m_mciOpenParms.lpstrElementName = m_psMidiFile; //要播放的MIDI
	m_mciOpenParms.wDeviceID = 0;

	// 打开
	m_dwReturn = mciSendCommand( 0, MCI_OPEN, MCI_OPEN_ELEMENT | MCI_OPEN_TYPE, (DWORD)(LPVOID)&m_mciOpenParms );
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoPlayMidi, MCI_OPEN: " );
		OutputDebugString( temp );
	}

	m_wDeviceID = m_mciOpenParms.wDeviceID;

	//播放
    m_mciPlayParms.dwCallback = (DWORD)m_hWnd;
	m_dwReturn = mciSendCommand(m_wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID)&m_mciPlayParms);

	if( m_dwReturn != 0 )
    {
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoPlayMidi, MCI_PLAY: " );
		OutputDebugString( temp );
        mciSendCommand( m_wDeviceID, MCI_CLOSE, 0, NULL );
        return( m_dwReturn );
    }

	//
    return( 0L );
};

//
void CMidi::DoReplayMidi()
{
	char temp[256];

	m_dwReturn = mciSendCommand( m_wDeviceID, MCI_SEEK, MCI_SEEK_TO_START, NULL);
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoReplayMidi, MCI_SEEK: " );
		OutputDebugString( temp );
	}

	m_dwReturn = mciSendCommand( m_wDeviceID, MCI_PLAY, MCI_NOTIFY, (DWORD)(LPVOID)&m_mciPlayParms);
	if( m_dwReturn != 0 )
	{
		mciGetErrorString( m_dwReturn, temp, 255 );
		OutputDebugString( "DoReplayMidi, MCI_PLAY: " );
		OutputDebugString( temp );
	}
}

//
void CMidi::DoStopMidi()
{
	char temp[256];

	if( m_wDeviceID != 0 )
	{
		m_dwReturn = mciSendCommand( m_wDeviceID, MCI_STOP, MCI_WAIT, NULL );
		if( m_dwReturn != 0 )
		{
			mciGetErrorString( m_dwReturn, temp, 255 );
			OutputDebugString( "DoStopMidi, MCI_STOP: " );
			OutputDebugString( temp );
		}

		m_dwReturn = mciSendCommand( m_wDeviceID, MCI_CLOSE, NULL, NULL );
		if( m_dwReturn != 0 )
		{
			mciGetErrorString( m_dwReturn, temp, 255 );
			OutputDebugString( "DoStopMidi, MCI_CLOSE: " );
			OutputDebugString( temp );
		}

		m_wDeviceID = 0;
	}
}

//
void CMidi::PlayWave(char* psFileName)
{
    if( !m_bDSoundOK )
    {
        // 如果DirectSound系统设置不成功，则使用默认方法播放声音文件
        // 该方法的缺点：不支持混响，同一时刻只能有一个声音文件在播放
	    sndPlaySound( psFileName, SND_ASYNC|SND_NODEFAULT );
        return;
    }

    // 下面使用DirectSound的方法播放声音
    m_nCurSound ++;
    if( m_nCurSound >= MAX_SOUND )
        m_nCurSound = 0;

    int i = m_nCurSound;

    if( m_pBufferArray[i] != NULL )
    {
        m_pBufferArray[i]->Stop();
        m_pBufferArray[i]->Release();
        m_pBufferArray[i] = NULL;
    }

    // 打开声音文件
    LPWAVEFORMATEX pwfe;
    DSBUFFERDESC   dsbd;
    DWORD          size;
    BYTE*          pBuf;

    m_pWave = new CWave( psFileName );
    if( !m_pWave->IsValid() )
    {
        m_bDSoundOK = FALSE;
        goto err;
    }

    pwfe = m_pWave->GetFormat();
    size = m_pWave->GetSize();
    pBuf = m_pWave->GetData();

    memset( &dsbd, 0, sizeof(DSBUFFERDESC));
    dsbd.dwSize = sizeof(DSBUFFERDESC);
    dsbd.dwBufferBytes = size;
    dsbd.lpwfxFormat = (LPWAVEFORMATEX) pwfe;
    dsbd.dwFlags = DSBCAPS_STATIC;

    if( m_pDSound->CreateSoundBuffer(&dsbd, &(m_pBufferArray[i]), NULL) != 0 )
        goto err;
		
	LPVOID ptr1;		//指针1
	LPVOID ptr2;		//指针2
	DWORD size1;		
	DWORD size2;

	if( ( m_pBufferArray[i]->Lock(0,size,&ptr1,&size1,&ptr2,&size2,0))!=DS_OK)
	{
		MessageBox(m_hWnd,"error Lock DSoundBuffer","error",MB_OK);
		goto err;
	}
	
	//拷贝数据到声音缓冲区
	memcpy(ptr1,pBuf,size1);
	if(size2) memcpy(ptr2,pBuf+size1,size2);
	
	if( m_pBufferArray[i]->Unlock(ptr1,size1,ptr2,size2)!=DS_OK)
	{
		MessageBox(m_hWnd,"error UnLock SoundBuffer","error",MB_OK);
		goto err;
	}

    // 播放
    m_pBufferArray[i]->SetCurrentPosition(0);
    m_pBufferArray[i]->Play(0,0,0);

err:
    delete m_pWave;
}

//the end.
