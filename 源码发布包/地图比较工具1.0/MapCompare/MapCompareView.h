
// MapCompareView.h : MapCompareView 类的接口
//


#pragma once

#define CAPACITY 10000//设定可以UNDO的次数
enum enum_left_or_right
{
	LEFT  = 1,
	RIGHT = 2,
};

enum enum_map_scale
{
	SCALE1 = 0,
	SCALE2 = 1,
	SCALE4 = 2,
	SCALE8 = 3,
};

// MapCompareView 窗口

class MapCompareView : public CWnd
{
private:
	CString GetPathName(void);

private:
	//与绘图有关的变量
	BOOL run_once;//用于OnDraw中只执行一次的程序段
	HBITMAP MapTile;//地图单元格位图
	CBitmap LeftBuff;
	CBitmap RightBuff;
	CDC m_LeftBuffDC;
	CDC m_RightBuffDC;
	CDC m_TileDC;
	CFont m_ViewFont;

	//与绘图有关的函数
	void displaymap(void);//显示用户区的所有信息
	void GetGridScale(int* scaled_w, int* scaled_h);
	void draw_map_ceil(int i, int x, int y, int w, int h, CDC *pbuffDC);//显示地图的一个方格
	void draw_table_lines();//显示画面上所有的分界线
	void drawrect(CRect *prc,CDC *pbuffDC);//在主缓（*pbuffDC）上绘制一个方块（*prc）
	void draw_edit_area(void);

private:
	//与undo功能有关的变量
	BOOL undo_st;//是否可以undo
	int m_bkup_counter;//定义计数器（undo的次数）
	int m_bkup_top;//定义栈顶指针（undo栈）
	int backup[CAPACITY][4];//用于存放对地图矩阵写入操作的历史数据，undo操作用
	//数据结构：队列，所以，m_bkup_counter的上限就是CAPACITY常数

	//与undo功能有关的函数
	void ResetBackup();//用于清空历史数据，并使undo_st指示失效。
	void save(enum_left_or_right lor, int value,int m_x_value,int m_y_value);//向地图矩阵写入一个数据
	BOOL Load(int* lor, int *pvalue,int *pm_x_value,int *pm_y_value);//从地图矩阵读出一个数据
	void Undo(void);//执行undo操作的函数

	//地图数据
	char map_left[MAP_GRID_W][MAP_GRID_H][2];//左边地图矩阵
	char map_right[MAP_GRID_W][MAP_GRID_H][2];//右边地图矩阵
	char map_diff[MAP_GRID_H][MAP_GRID_H];//差异矩阵

	int ReadMapLeft(int x, int y);//从地图矩阵x,y的位置读出一个数值
	void WriteMapLeft(int x, int y, int z);//向地图矩阵x,y的位置写入z
	int ReadMapRight(int x, int y);//从地图矩阵x,y的位置读出一个数值
	void WriteMapRight(int x, int y, int z);//向地图矩阵x,y的位置写入z

	CString m_sLeftFileName;//文件名(带路径)
	BOOL m_bLeftModified;//是否被修改过的标志
	CString m_sRightFileName;
	BOOL m_bRightModified;

	void SaveLeftFile(void);
	void LoadLeftFile(void);
	void SaveRightFile(void);
	void LoadRightFile(void);
	void Compare(void);
	void RenewTitle(void);

	void SetMapCell(enum_left_or_right lor, int x, int y, int value);

	//滚动条
	CScrollBar m_scoLeftAreaH;
	CScrollBar m_scoLeftAreaV;
	CScrollBar m_scoRightAreaH;
	CScrollBar m_scoRightAreaV;

	//与编辑区域有关的变量
	CPoint m_ptDragStart;//开始拖拽的点
	CRect m_rcMouseSelectArea;
	CRect m_rcSelectedGridArea;
	CPoint m_ptAreaTopLeft;

	//当前编辑区的显示比例
	int m_nMapScale;

	//鼠标左键按下的标志
	BOOL m_bLButtonDown;



// 构造
public:
	MapCompareView();

// 特性
public:

// 操作
public:

// 重写
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~MapCompareView();

	// 生成的消息映射函数
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnOpenLeft();
	afx_msg void OnOpenRight();
	afx_msg void OnSaveLeft();
	afx_msg void OnSaveRight();
	afx_msg void OnUpdateSaveLeft(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSaveRight(CCmdUI *pCmdUI);
	afx_msg void OnScale1();
	afx_msg void OnScale2();
	afx_msg void OnScale4();
	afx_msg void OnScale8();
	afx_msg void OnUpdateScale1(CCmdUI *pCmdUI);
	afx_msg void OnUpdateScale2(CCmdUI *pCmdUI);
	afx_msg void OnUpdateScale4(CCmdUI *pCmdUI);
	afx_msg void OnUpdateScale8(CCmdUI *pCmdUI);
	afx_msg void OnCopyToLeft();
	afx_msg void OnCopyToRight();
	afx_msg void OnUpdateCopyToLeft(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCopyToRight(CCmdUI *pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnUp();
	afx_msg void OnDown();
	afx_msg void OnLeft();
	afx_msg void OnRight();
};

